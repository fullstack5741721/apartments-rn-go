package utils

import (
	"fmt"

	"github.com/go-playground/validator/v10"
	"github.com/kataras/iris/v12"
)

// pass in status code and append title and detail err message
func CreateError(statusCode int, title string, detail string, ctx iris.Context) {
	ctx.StopWithProblem(statusCode, iris.NewProblem().Title(title).Detail(detail))
}

// internal 500 error
func CreateInternalServerError(ctx iris.Context) {
	CreateError(iris.StatusInternalServerError,
		"Internal Server Error",
		"Internal Server Error",
		ctx,
	)
}

func HandleValidationErrors(err error, ctx iris.Context) {
	if errs, ok := err.(validator.ValidationErrors); ok {
		validationErrors := wrapValidationErrors(errs)
		// log error
		fmt.Println("validationErrors", validationErrors)
		// convert errors to suitable format
		ctx.StopWithProblem(
			iris.StatusBadRequest, // 400 err
			iris.NewProblem().
				Title("Validation error").
				Detail("One or more fields failed to be validated").
				Key("errors", validationErrors))

		return
	}

	fmt.Print(err)
	// It's probably an internal JSON error, let's not give more info here.
	CreateInternalServerError(ctx)
}

// returns slice of validation error structs
func wrapValidationErrors(errs validator.ValidationErrors) []validationError {
	validationErrors := make([]validationError, 0, len(errs)) // initialize empty slice
	for _, validationErr := range errs {
		validationErrors = append(validationErrors, validationError{
			ActualTag: validationErr.ActualTag(),
			Namespace: validationErr.Namespace(),
			Kind:      validationErr.Kind().String(),
			Type:      validationErr.Type().String(),
			Value:     fmt.Sprintf("%v", validationErr.Value()),
			Param:     validationErr.Param(),
		})
	}

	return validationErrors
}

type validationError struct {
	ActualTag string `json:"tag"`
	Namespace string `json:"namespace"`
	Kind      string `json:"kind"`
	Type      string `json:"type"`
	Value     string `json:"value"`
	Param     string `json:"param"`
}