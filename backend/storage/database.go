package storage

import (
	"apartments-server/models"
	"log"
	"os"

	"github.com/joho/godotenv"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var DB *gorm.DB

// returns gorm db pointer
func connectToDb() *gorm.DB {
	err := godotenv.Load() // load env vars
	if err != nil {
		panic("Error loading.env file")
	}

	dsn := os.Getenv("DB_CONNECTION_STRING")
	// returns db pointer and error
	db, dbError := gorm.Open(postgres.Open(dsn), &gorm.Config{}) // connect to db
	if dbError != nil {
		log.Panic("Error connection to db")
	}

	DB = db
	return db
}

// takes in db pointer
func performMigrations(db *gorm.DB) {
	db.AutoMigrate(
		&models.User{},
	)
}
// initialize database
func InitializeDb() *gorm.DB {
	db := connectToDb()
	performMigrations(db)
	return db
}