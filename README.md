<h1 align="center">
<br>
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon@latest/icons/go/go-original-wordmark.svg" width="80px" />
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon@latest/icons/react/react-original-wordmark.svg" width="80px" />
<br>
<br>
Fullstack Apartments Clone - <a href="https://www.apartments.com" style="color:#ADD8E6;">www.apartments.com</a>
</h1>

<h4 align="center">React Native(Expo)/React Query/Typescript/Go(Iris)/Postgres</h4>

<div>
    <img src="./phone.gif" width="230px" height="380px" style="margin-right: 20px;">
    <div style="display: inline-block; text-align: left;">
        <h3>Client Side</h3>
        <ul>
            <li>Push notifications</li>
            <li>Google maps</li>
            <li>Login with Google/Facebook/Apple</li>
            <li>Login and register with an email and password</li>
            <li>Forgot user password and reset user password screens</li>
            <li>Star based reviews</li>
            <li>React Query for fetching data</li>
            <li>Chat/messaging screens</li>
            <li>Detailed screens to display data</li>
            <li>Airbnb like carousel cards for displaying apartment images</li>
        </ul>
    </div>
    <div style="display: inline-block; text-align: left; margin-left: 40px;">
        <h3>Backend / Database</h3>
        <ul>
            <li>GO API with postgres</li>
            <li>Sends notifications to users</li>
            <li>Uploads and store images to an S3 Bucket</li>
            <li>Implements redis, JWTs, chat via Socket.io</li>
            <li>Login with Google/Facebook/Apple</li>
            <li>Login and Register with an email and password</li>
            <li>Deploy via Docker and Dockerhub</li>
            <li>Uses NGINX and Letsencrypt to enable https connections</li>
        </ul>
    </div>
</div>

<br>

<div align="center">
    <img src="./frontend1.png" width="80%" height="240px" style="margin-bottom: 20px;">
</div>

<div align="center">
    <img src="./frontend2.png" width="80%" height="240px">
</div>
