import {
  Platform,
  StyleSheet,
  View,
  ScrollView,
  FlatList,
  TouchableOpacity,
  Text,
} from "react-native";
import { useState } from "react";
import { useNavigation } from "@react-navigation/native";
import { useQueryClient } from "react-query";
import { Input } from "@ui-kitten/components";
import { Row } from "../components/Row";
import { Screen } from "../components/Screen";
import { ModalHeader } from "../components/ModalHeader";
import { theme } from "../theme";
import { getSuggestedLocations } from "../services/location";
import { Location } from "../types/locationIQ";
import { CurrentLocationButton } from "../components/CurrentLocationButton";
import { getFormattedLocationText } from "../utils/getFormattedLocationText";
import { RecentSearchList } from "../components/RecentSearchList";
import { SearchAddress } from "../components/SearchAddress";

export const FindLocationsScreen = () => {
  const navigation = useNavigation();
  // const queryClient = useQueryClient();
  // const recentSearches: Location[] | undefined =
  //   queryClient.getQueryData("recentSearches");
  const [value, setValue] = useState("");
  const [suggestions, setSuggestions] = useState<Location[]>([]);

  // get our suggestions via suggestedLocations req upon typing
  const handleChange = async (val: string) => {
    // set input value
    // set suggestions to arr of results
    setValue(val);
    if (val.length > 2) {
      const locations = await getSuggestedLocations(val);
      if (locations.length > 0) setSuggestions(locations);
    } else if (val.length === 0) setSuggestions([]);
  };

  // call api if search is edited, if no results found navigate back
  const handleSubmitEditing = async () => {
    const locations = await getSuggestedLocations(value);
    if (locations.length > 0) {
      console.log("navigate to search screen passing in ", locations[0]);
    }
  };

  // navigate back to the search screem with a certain location/region to display when opening the map
  const handleNavigate = (location: Location) => {
    // setRecentSearch(location);
    navigation.navigate("Root", {
      screen: "Search",
      params: {
        location: getFormattedLocationText(location, "autocomplete"),
        lat: location.lat,
        lon: location.lon,
        boundingBox: location.boundingbox,
      },
    });
  };

  // ios/android search input
  const getInput = () => {
    if (Platform.OS === "ios") {
      return (
        <Input
          keyboardType="default"
          autoFocus
          selectionColor={theme["color-primary-500"]}
          placeholder="Enter Location"
          size="large"
          onChangeText={handleChange}
          onSubmitEditing={handleSubmitEditing}
          style={styles.defaultMarginTop}
        />
      );
    }

    return (
      <Row>
        <Input
          keyboardType="default"
          autoFocus
          selectionColor={theme["color-primary-500"]}
          placeholder="Enter Location"
          size="large"
          onChangeText={handleChange}
          onSubmitEditing={handleSubmitEditing}
          style={[styles.defaultMarginTop, { width: "80%" }]}
        />
      </Row>
    );
  };
  // const setRecentSearch = (location: Location) => {
  //   queryClient.setQueryData("recentSearches", () => {
  //     if (recentSearches) {
  //       let included = false;
  //       for (let i of recentSearches) {
  //         if (
  //           i.display_name === location.display_name &&
  //           i.lon === location.lon &&
  //           i.lat === location.lat
  //         ) {
  //           included = true;
  //           break;
  //         }
  //       }
  //       if (!included) return [location, ...recentSearches];
  //       return recentSearches;
  //     }
  //     return [location];
  //   });
  // };

  // text component with suggested locations to show when typing
  const SuggestedText = ({ locationItem }: { locationItem: Location }) => {
    // formatted suggested locations text
    const location = getFormattedLocationText(locationItem, "autocomplete");
    return (
      <Row style={styles.suggestionContainer}>
        <Text>{location}</Text>
      </Row>
    );
  };

  return (
    <Screen>
      {Platform.OS === "ios" ? <ModalHeader /> : null}
      <View style={styles.screenContent}>
        {/* input box */}
        {getInput()}
        {/* show suggested places based on search value */}
        {suggestions.length > 0 ? (
          <FlatList
            showsVerticalScrollIndicator
            data={suggestions}
            renderItem={({ item, index }) => (
              <TouchableOpacity onPress={() => handleNavigate(item)}>
                <SuggestedText locationItem={item} />
              </TouchableOpacity>
            )}
          />
        ) : (
          // display searched locations
          <ScrollView bounces={false}>
            <CurrentLocationButton style={styles.currentLocationButton} />
            {/* <RecentSearchList
              style={styles.recentSearchContainer}
              recentSearches={recentSearches}
            /> */}
          </ScrollView>
        )}

        {/*<SearchAddress*/}
        {/*  type="autocomplete"*/}
        {/*  handleGoBack={navigation.goBack}*/}
        {/*  suggestions={suggestions}*/}
        {/*  setSuggestions={(item) => setSuggestions(item as Location[])}*/}
        {/*  handleSuggestionPress={(item) => handleNavigate(item as Location)}*/}
        {/*/>*/}
        {/*{suggestions.length === 0 ? (*/}
        {/* <ScrollView bounces={false}>*/}
        {/*    <CurrentLocationButton style={styles.currentLocationButton} />*/}
        {/*    <RecentSearchList*/}
        {/*      style={styles.recentSearchContainer}*/}
        {/*      recentSearches={recentSearches}*/}
        {/*    />*/}
        {/*  </ScrollView> */}
        {/*) : null}*/}
      </View>
    </Screen>
  );
};

const styles = StyleSheet.create({
  screenContent: {
    marginHorizontal: 10,
  },
  defaultMarginTop: {
    marginTop: 10,
  },
  suggestionContainer: {
    alignItems: "center",
    padding: 15,
    borderBottomWidth: 1,
    borderBottomColor: theme["color-gray"],
  },
  currentLocationButton: {
    marginTop: 40,
  },
  recentSearchContainer: { marginTop: 30 },
});
