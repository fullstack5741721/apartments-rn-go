import { StyleSheet, View, ViewStyle } from "react-native";

// returns a row for reusability
export const Row = ({
  children,
  style,
}: {
  children: any;
  style?: ViewStyle | ViewStyle[]; // accept optional/additional styles
}) => {
  return <View style={[styles.container, style]}>{children}</View>;
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
  },
});
