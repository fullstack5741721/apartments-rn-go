import MapView, { Region } from "react-native-maps";
import { View, StyleSheet, Platform, TouchableOpacity } from "react-native";
import { useState, useEffect, useRef } from "react";
import { useNavigation } from "@react-navigation/native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { useQuery } from "react-query";
import { Button } from "@ui-kitten/components";
import axios from "axios";

import { Property } from "../types/property";
import { MapMarker } from "./MapMarker";
import { theme } from "../theme";
import { Card } from "./Card";
import { getPropertiesInArea } from "../data/properties";
import { endpoints, queryKeys } from "../constants";
import { useSearchPropertiesQuery } from "../hooks/queries/useSearchPropertiesQuery";

// used to persist the region if search area from the map
let mapRegion: Region | undefined = undefined;

export const Map = ({
  properties,
  mapRef,
  // location,
  // setLocation,
  initialRegion,
}: {
  properties: Property[];
  mapRef: React.MutableRefObject<MapView | null>;
  // location: string;
  // setLocation: (location: string) => void;
  initialRegion?: Region | undefined;
}) => {
  const [activeIndex, setActiveIndex] = useState(-1);
  // const mapRef = useRef<MapView | null>(null)
  // const [showSearchAreaButton, setShowSearchAreaButton] = useState(false);
  // const [boundingBox, setBoundingBox] = useState<number[]>([]); // used for searching properties in region
  // const [region, setRegion] = useState<Region | undefined>(
  //   mapRegion ? mapRegion : undefined
  // );
  const navigation = useNavigation();
  //
  // const searchProperties = useSearchPropertiesQuery(boundingBox);

  // useEffect(() => {
  //   if (location === "Map Area") return;
  //
  //   if (initialRegion) {
  //     setShowSearchAreaButton(false);
  //     setRegion(initialRegion);
  //   }
  // }, [initialRegion]);

  // select/deselect the property card when pressing on a marker
  const unFocusProperty = () => {
    setActiveIndex(-1);
    navigation.setOptions({ tabBarStyle: { display: "flex" } });
  };
  const handleMapPress = () => {
    if (Platform.OS === "android") unFocusProperty();
  };

  // map marker press event - smoothly animate camera to new position using long/lat
  const handleMarkerPress = (index: number) => {
    if (Platform.OS === "ios") {
      setTimeout(() => {
        mapRef.current?.animateCamera({
          center: {
            latitude: properties[index].lat,
            longitude: properties[index].lng,
          },
        });
      }, 100);
    }
    setActiveIndex(index);
    navigation.setOptions({ tabBarStyle: { display: "none" } });
  };
  // activate index to get the correct property out of the array, hide bottom nav bar to open up the screen more
  // const handleMarkerPress = (index: number) => {
  //   setTimeout(() => {
  //     mapRef.current?.animateCamera({
  //       center: {
  //         latitude: properties[index].lat,
  //         longitude: properties[index].lng,
  //       },
  //     });
  //   }, 100);
  //   setTimeout(() => {
  //     const newRegion: Region = {
  //       latitude: properties[index].lat,
  //       latitudeDelta:
  //         region?.latitudeDelta && region.latitudeDelta < 4
  //           ? region.latitudeDelta
  //           : 4,
  //       longitude: properties[index].lng,
  //       longitudeDelta:
  //         region?.longitudeDelta && region.longitudeDelta < 4
  //           ? region.longitudeDelta
  //           : 4,
  //     };
  //
  //     setRegion(newRegion);
  //   }, 600);
  //
  //   setActiveIndex(index);
  //   navigation.setOptions({ tabBarStyle: { display: "none" } });
  // };

  // const handleSearchAreaButtonPress = () => {
  //   searchProperties.refetch();
  //   setLocation("Map Area");
  //   mapRegion = region;
  //   setShowSearchAreaButton(false);
  // };

  // DOUBLE CLICK ON IOS TO SEE THE MARKERS
  // const properties = [
  //   {
  //     ID: 1,
  //     images: [
  //       "https://cimg8.ibsrv.net/ibimg/www.apartmentratings.com/1134x456_85-1/7/0/1/701089811589111106751078071.jpg",
  //       "https://res.cloudinary.com/g5-assets-cld/image/upload/x_823,y_0,h_4915,w_6144,c_crop/q_auto,f_auto,fl_lossy,g_center,h_400,w_500/g5/g5-c-5m5d9vuh1-harbor-group-international-single-domain-client/g5-cl-1mbwl09zzd-harbor-group-international-single-domain-client-miami-fl/uploads/Parkline_09_vorj3l.jpg",
  //     ],
  //     rentLow: 3750,
  //     rentHigh: 31054,
  //     bedroomLow: 1,
  //     bedroomHigh: 5,
  //     name: "The Hamilton",
  //     street: "555 NE 34th St",
  //     city: "Miami",
  //     state: "Florida",
  //     zip: 33137,
  //     tags: ["Parking", "Value0", "Value1", "Value2", "Value3", "Value4"],
  //     lat: 25.80913,
  //     lng: -80.186363,
  //   },
  //   {
  //     ID: 2,
  //     images: [
  //       "https://cimg8.ibsrv.net/ibimg/www.apartmentratings.com/1134x456_85-1/7/0/1/701089811589111106751078071.jpg",
  //       "https://res.cloudinary.com/g5-assets-cld/image/upload/x_823,y_0,h_4915,w_6144,c_crop/q_auto,f_auto,fl_lossy,g_center,h_400,w_500/g5/g5-c-5m5d9vuh1-harbor-group-international-single-domain-client/g5-cl-1mbwl09zzd-harbor-group-international-single-domain-client-miami-fl/uploads/Parkline_09_vorj3l.jpg",
  //     ],
  //     rentLow: 3750,
  //     rentHigh: 31054,
  //     bedroomLow: 1,
  //     bedroomHigh: 5,
  //     name: "The Hamilton",
  //     street: "555 NE 34th St",
  //     city: "Miami",
  //     state: "Florida",
  //     zip: 33137,
  //     tags: ["Parking", "Value0", "Value1", "Value2", "Value3", "Value4"],
  //     lat: 25.78354,
  //     lng: -80.21391,
  //   },
  // ];

  return (
    <View style={styles.container}>
      <MapView
        style={styles.map}
        userInterfaceStyle="light"
        ref={mapRef}
        onPress={handleMapPress}
        initialRegion={initialRegion ? initialRegion : undefined}
      >
        {/*marker pins to display for each property*/}
        {/*{properties.map((property, index) => <MapMarker key={property.ID} lat={property.lat} lng={property.lng} color={activeIndex === index ? theme['color-info-400'] : theme['color-primary-500']} onPress={() => handleMarkerPress(index)} />)}*/}
        {/*</MapView>*/}
        {/*{activeIndex > -1 && <Card property={properties[activeIndex]} style={styles.card}/>}*/}
        {/*<MapView*/}
        {/*  provider={"google"}*/}
        {/*  style={styles.map}*/}
        {/*  userInterfaceStyle={"light"}*/}
        {/*  ref={mapRef}*/}
        {/*  onPress={handleMapPress}*/}
        {/*  region={region}*/}
        {/*  onRegionChangeComplete={(region, isGesture) => {*/}
        {/*    if (isGesture?.isGesture) {*/}
        {/*      if (!showSearchAreaButton) setShowSearchAreaButton(true);*/}

        {/*      const newBoundingBox = [*/}
        {/*        region.latitude - region.latitudeDelta / 2,*/}
        {/*        region.latitude + region.latitudeDelta / 2,*/}
        {/*        region.longitude - region.longitudeDelta / 2,*/}
        {/*        region.longitude + region.longitudeDelta / 2,*/}
        {/*      ];*/}
        {/*      setRegion(region);*/}
        {/*      setBoundingBox(newBoundingBox);*/}
        {/*    }*/}
        {/*  }}*/}
        {/*>*/}
        {properties &&
          properties.map((i, index) => (
            <MapMarker
              key={index}
              lat={i.lat}
              lng={i.lng}
              color={
                activeIndex === index
                  ? theme["color-info-400"]
                  : theme["color-primary-500"]
              }
              onPress={() => handleMarkerPress(index)}
            />
          ))}
      </MapView>
      {activeIndex > -1 && (
        <>
          {Platform.OS === "ios" && (
            <TouchableOpacity style={styles.exit} onPress={unFocusProperty}>
              <MaterialCommunityIcons
                name="close"
                color={theme["color-primary-500"]}
                size={24}
              />
            </TouchableOpacity>
          )}
          <Card
            property={properties[activeIndex]}
            style={styles.card}
            onPress={() =>
              navigation.navigate("PropertyDetails", {
                propertyID: properties[activeIndex].ID,
              })
            }
          />
        </>
      )}

      {/*{showSearchAreaButton && activeIndex === -1 && (*/}
      {/*  <Button*/}
      {/*    style={styles.searchAreaButton}*/}
      {/*    appearance={"ghost"}*/}
      {/*    onPress={handleSearchAreaButtonPress}*/}
      {/*  >*/}
      {/*    Search Area*/}
      {/*  </Button>*/}
      {/*)}*/}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    overflow: "hidden",
  },
  map: {
    height: "100%",
    width: "100%",
  },
  card: {
    position: "absolute",
    bottom: 10,
    height: 360, // remove afterwards as this wasnt here
  },
  exit: {
    backgroundColor: "#fff",
    padding: 10,
    position: "absolute",
    top: 170,
    left: 15,
    borderRadius: 30,
  },
  searchAreaButton: {
    position: "absolute",
    bottom: 30,
    zIndex: 100,
    borderRadius: 30,
    alignSelf: "center",
    backgroundColor: "white",
    borderColor: theme["color-gray"],
    borderWidth: 1,
  },
});
