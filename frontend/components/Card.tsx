import React, { useState, useRef } from "react";
import {
  Pressable,
  ViewStyle,
  StyleSheet,
  TouchableOpacity,
  View,
  Modal,
  Dimensions,
  FlatList,
  Image,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { useMutation, useQueryClient } from "react-query";
import axios from "axios";
import { endpoints } from "../constants";
import { useLoading } from "../hooks/useLoading";
import { useDeletePropertyMutation } from "../hooks/mutations/useDeletePropertyMutation";

import { Screen } from "./Screen"; // global wrapper
import { ImageCarousel } from "./ImageCarousel";
import { Row } from "./Row";
import { CardInformation } from "./CardInformation";
import { Text, Button } from "@ui-kitten/components";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { theme } from "../theme";
import { LISTMARGIN } from "../constants";
import { Property } from "../types/property";

export const Card = ({
  property,
  style,
}: {
  // prop types
  property: Property;
  style?: ViewStyle;
}) => {
  return (
    <View style={[styles.container, styles.boxShadow, style]}>
      {/* applies left/right icons to image to use as a carousel */}
      <ImageCarousel chevronsShown images={property.images} />
      {/* Property info content */}
      <View style={styles.informationContainer}>
        <Row style={styles.rowJustification}>
          <Text category="s1">
            ${property.rentLow.toLocaleString()} - $
            {property.rentHigh.toLocaleString()}
          </Text>
          {/*<MaterialCommunityIcons*/}
          {/*  name="heart-outline"*/}
          {/*  color={theme["color-primary-500"]}*/}
          {/*  size={24}*/}
          {/*/>*/}
          <MaterialCommunityIcons
              name="heart"
              color={theme["color-primary-500"]}
              size={24}
          />
        </Row>
        <Text category="c1">
          {property.bedroomLow} - {property.bedroomHigh} Beds
        </Text>
        <Text category="c1" style={styles.defaultMarginTop}>
          {property.name}
        </Text>
        <Text category="c1" style={styles.defaultMarginTop}>
          {property.street}
        </Text>
        <Text category="c1" style={styles.defaultMarginTop}>
          {property.city}, {property.state}, {property.zip}
        </Text>
        <Text category="c1" style={styles.defaultMarginTop}>
          {property.tags.map((tag, index) =>
            index === property.tags.length - 1 ? tag : `${tag}, `
          )}
        </Text>
        <Row style={[styles.defaultMarginTop, styles.rowJustification]}>
          <Button
            appearance="ghost"
            style={[
              {
                borderColor: theme["color-primary-500"],
              },
              styles.button,
            ]}
            size="small"
            onPress={console.log("email the properties")}
          >
            Email
          </Button>
          <Button
            style={styles.button}
            size="small"
            onPress={console.log("email the properties")}
          >
            Call
          </Button>
        </Row>
      </View>
    </View>
  );
};

// export const Card = ({
//   property,
//   onPress,
//   myProperty,
//   style,
// }: {
//   property: Property;
//   onPress?: () => void;
//   myProperty?: boolean;
//   style?: ViewStyle;
// }) => {
//   const navigation = useNavigation();
//   const [showModal, setShowModal] = useState(false);
//   const openModal = () => setShowModal(true);
//   const closeModal = () => setShowModal(false);
//   const deleteProperty = useDeletePropertyMutation();

//   const handleEditProperty = () => {
//     navigation.navigate("EditProperty", { propertyID: property.ID });
//     closeModal();
//   };

//   const handleDeleteProperty = () => {
//     deleteProperty.mutate({ propertyID: property.ID });
//     closeModal();
//   };

//   return (
//     <Pressable
//       onPress={onPress}
//       style={[styles.container, styles.boxShadow, style]}
//     >
//       <ImageCarousel
//         onImagePress={onPress}
//         images={property.images}
//         chevronsShown
//       />
//       <CardInformation property={property} myProperty={myProperty} />

//       {myProperty ? (
//         <TouchableOpacity onPress={openModal} style={styles.ellipses}>
//           <MaterialCommunityIcons
//             name="dots-horizontal"
//             color={theme["color-primary-500"]}
//             size={30}
//           />
//         </TouchableOpacity>
//       ) : null}

//       <Modal visible={showModal} transparent>
//         <View style={[styles.modal, styles.boxShadow]}>
//           <Button
//             status={"info"}
//             appearance="ghost"
//             onPress={handleEditProperty}
//           >
//             Edit Property
//           </Button>

//           <Button
//             status={"danger"}
//             appearance="ghost"
//             onPress={handleDeleteProperty}
//           >
//             Delete Property
//           </Button>
//           <Button appearance="ghost" onPress={closeModal}>
//             Cancel
//           </Button>
//         </View>
//       </Modal>
//     </Pressable>
//   );
// };

const styles = StyleSheet.create({
  defaultMarginTop: {
    marginTop: 5,
  },
  button: {
    width: "49%",
  },
  rowJustification: {
    justifyContent: "space-between",
  },
  container: {
    marginHorizontal: LISTMARGIN,
    borderRadius: 5,
    backgroundColor: "white",
  },
  ellipses: {
    backgroundColor: "#fff",
    position: "absolute",
    borderRadius: 5,
    paddingHorizontal: 5,
    top: 10,
    right: 15,
  },
  backdrop: {
    backgroundColor: "rgba(0, 0, 0, 0.5)",
  },
  modal: {
    backgroundColor: "#fff",
    borderRadius: 5,
    padding: 20,
    position: "absolute",
    top: Dimensions.get("screen").height / 3,
    right: Dimensions.get("screen").width / 4,
  },
  boxShadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },
  informationContainer: {
    paddingVertical: 10,
    paddingHorizontal: 5,
    borderColor: "#d3d3d3",
    borderWidth: 1,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
});
