import {
  SafeAreaView,
  StyleSheet,
  ViewStyle,
  Platform,
  StatusBar,
} from "react-native";

// import { Loading } from "./Loading";
// import { useLoading } from "../hooks/useLoading";

// Global wrapper for screens. For basic styling - render content or loading spinner
export const Screen = ({
  children,
  style,
}: {
  children: any;
  style?: ViewStyle; // allow to pass in any specific styles for each screen
}) => {
  // const { loading } = useLoading();

  return (
    <SafeAreaView style={[styles.container, style]}>
      <StatusBar barStyle={"dark-content"} />
      {/* {loading ? <Loading /> : children} */}
      {children}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
});
